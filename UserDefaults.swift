//
//  UserDefaults.swift
//  MoneyWiser
//
//  Created by Kristopher on 4/14/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let languagePreference = DefaultsKey<String?>("languagePreference")
    static let displayCurrency = DefaultsKey<String?>("displayCurrency")
    static let enableNotifications = DefaultsKey<Bool?>("enableNotifications")
}
