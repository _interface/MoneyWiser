//
//  SpendingViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import FSCalendar
import Chameleon
import RealmSwift
import Timepiece

class ExpenditureViewController: UIViewController{


    fileprivate var transactionTimestamp = [String]()
    fileprivate var transactionTypes = [String]()
    fileprivate var transactionAmounts = [String]()
    fileprivate var balanceTypes = [Bool]()
    fileprivate var paymentMethods = [String]()
    fileprivate weak var calendar: FSCalendar!
    var cashFlowTimelineTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        UIApplication.shared.statusBarStyle = .default
        view.backgroundColor = Color.grey.lighten5
        registerNotifications()
        prepareCalendarView()
        prepareCashflowTimelineTableView()
        preparePersistentStoreRetreival(date: nil)
        // Do any additional setup after loading the view.
    }
    
}

extension ExpenditureViewController {
    
    fileprivate func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateTransactionEntry), name: NSNotification.Name("transactionUpdated"), object: nil)
    }
    
    fileprivate func preparePersistentStoreRetreival(date: Date?){
        let realm = try! Realm()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        if date == nil {
            let transactions = realm.objects(Transaction.self).filter("date == %@", calendar.startOfDay(for: Date()))
            transactionTimestamp = transactions.map{$0.timestamp}
            print(transactions.map{$0.date})
            print(transactionTimestamp)
            transactionTypes = transactions.map{$0.reason}
            transactionAmounts = transactions.map{Helper.cleanDollars("\($0.amount)")}
            balanceTypes = transactions.map{$0.isExpense}
            paymentMethods = transactions.map{$0.payment}
        }
        else {
            guard let selectedDate = date else {
                return
            }
//            let dayBefore = selectedDate - 1.day
            print(calendar.startOfDay(for: selectedDate))
            let transactions = realm.objects(Transaction.self).filter("date == %@", calendar.startOfDay(for: (selectedDate + 1.day)!))
            transactionTimestamp = transactions.map{$0.timestamp}
            transactionTypes = transactions.map{$0.reason}
            transactionAmounts = transactions.map{Helper.cleanDollars("\($0.amount)")}
            balanceTypes = transactions.map{$0.isExpense}
            paymentMethods = transactions.map{$0.payment}
        }
        cashFlowTimelineTableView.reloadData()
    }
    
    @objc fileprivate func updateTransactionEntry(){
        preparePersistentStoreRetreival(date: calendar.selectedDate)
    }
    
    internal func prepareCashflowTimelineTableView(){
        cashFlowTimelineTableView = UITableView(frame: CGRect(x: 0, y: calendar.frame.maxY+4, width: view.frame.width, height: 300))
        cashFlowTimelineTableView.register(UINib(nibName: "transactionCell", bundle: nil) , forCellReuseIdentifier: "dailyTransactionViewCell")
        cashFlowTimelineTableView.delegate = self
        cashFlowTimelineTableView.dataSource = self
        cashFlowTimelineTableView.separatorStyle = .none
        view.addSubview(cashFlowTimelineTableView)
    }
    
    internal func prepareCalendarView(){
        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.38))
        calendar.dataSource = self
        calendar.delegate = self
        calendar.appearance.todayColor = MWStyles.themeColor
        calendar.appearance.weekdayTextColor = UIColor.init(hexString: "45CAE6")
        calendar.appearance.headerTitleColor = UIColor.init(hexString: "45CAE6")
        view.addSubview(calendar)
        self.calendar = calendar
    }
}

extension ExpenditureViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionTypes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dailyTransactionViewCell")
        let transactionTypeIndicator = cell?.viewWithTag(10) as! UIImageView
        let transactionTitle = cell?.viewWithTag(11) as! UILabel
        (cell?.viewWithTag(12) as! UILabel).font = UIFont.fontAwesome(ofSize: 15)
        (cell?.viewWithTag(12) as! UILabel).text = String.fontAwesomeIcon(name: .clockO)
        let timestampLabel = cell?.viewWithTag(13) as! UILabel
        let transactionIdentifier = cell?.viewWithTag(16)
        let transactionArrowIndicator = cell?.viewWithTag(14) as! UILabel
        let transactionAmountLabel = cell?.viewWithTag(15) as! UILabel
        (cell?.viewWithTag(20) as! UILabel).font = UIFont.fontAwesome(ofSize: 15)
        (cell?.viewWithTag(20) as! UILabel).text = String.fontAwesomeIcon(name: .exchange)
        let paymentDescription = cell?.viewWithTag(21) as! UILabel
        paymentDescription.text = paymentMethods[indexPath.row]
        switch transactionTypes[indexPath.row] {
        case "Transportation":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .bus, textColor: UIColor.flatYellow(), size: CGSize(width: 32, height: 32))
        case "Food":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .cutlery, textColor: UIColor.flatOrange(), size: CGSize(width: 32, height: 32))
        case "Purchases":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .shoppingBag, textColor: UIColor.flatMint(), size: CGSize(width: 32, height: 32))
        case "Leisure":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .coffee, textColor: UIColor.flatBrownColorDark(), size: CGSize(width: 32, height: 32))
        case "Health":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .heartbeat, textColor: UIColor.flatWatermelonColorDark(), size: CGSize(width: 32, height: 32))
        case "Tuition":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .book, textColor: UIColor.flatPurple(), size: CGSize(width: 32, height: 32))
        case "Pocket Money":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .money, textColor: UIColor.flatGreen(), size: CGSize(width: 32, height: 32))
        case "Salary":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .money, textColor: UIColor.flatGreen(), size: CGSize(width: 32, height: 32))
        case "Red Pocket":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .envelope, textColor: UIColor.flatRed(), size: CGSize(width: 32, height: 32))
        case "Scholarship":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .graduationCap, textColor: UIColor.flatBlueColorDark(), size: CGSize(width: 32, height: 32))
        case "Other":
            transactionTypeIndicator.image = UIImage.fontAwesomeIcon(name: .money, textColor: UIColor.flatGreen(), size: CGSize(width: 32, height: 32))
        default:
            break
        }
        transactionTitle.text = transactionTypes[indexPath.row]
        timestampLabel.text = transactionTimestamp[indexPath.row]
        transactionIdentifier?.backgroundColor = balanceTypes[indexPath.row] ? UIColor.flatWatermelon() : UIColor.flatMintColorDark()
        transactionArrowIndicator.font = UIFont.fontAwesome(ofSize: 15)
        transactionArrowIndicator.text = balanceTypes[indexPath.row] ? String.fontAwesomeIcon(name: .caretDown) : String.fontAwesomeIcon(name: .caretUp)
        transactionArrowIndicator.textColor = balanceTypes[indexPath.row] ?  MWStyles.expenseColor : MWStyles.incomeColor
        transactionAmountLabel.text = transactionAmounts[indexPath.row]
        transactionAmountLabel.textColor = balanceTypes[indexPath.row] ? MWStyles.expenseColor : MWStyles.incomeColor
        cell?.viewWithTag(9)?.isHidden = (indexPath.row == 3)
        cell?.separatorInset = .zero
        cell?.preservesSuperviewLayoutMargins = false
        cell?.layoutMargins = .zero
        cell?.selectionStyle = .none
        return cell!
    }
}

extension ExpenditureViewController :  FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        preparePersistentStoreRetreival(date: date)
    }
}
