//
//  MWEditToolbarViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/31/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material

class MWEditToolbarViewController: ToolbarController {

    fileprivate var doneButton: IconButton!
    fileprivate var backButton: IconButton!
    
    open override func prepare() {
        super.prepare()
        self.title = self.rootViewController.title
        prepareDoneButton()
        prepareBackButton()
        prepareStatusBar()
        prepareToolbar()
    }

}

extension MWEditToolbarViewController {
    fileprivate func prepareDoneButton(){
        doneButton = IconButton(image: Icon.cm.check, tintColor: .white)
        doneButton.pulseColor = .white
        doneButton.addTarget(self, action: #selector(doneButtonDidClicked(button:)), for: .touchUpInside)
    }
    
    fileprivate func prepareBackButton(){
        backButton = IconButton(image: Icon.cm.arrowBack, tintColor: .white)
        backButton.pulseColor = .white
        backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
    }
    
    fileprivate func prepareStatusBar(){
        statusBarStyle = .lightContent
        statusBar.backgroundColor = UIColor(hexString: "0AC775")
    }
    
    fileprivate func prepareToolbar() {
        
        toolbar.depthPreset = .none
        toolbar.backgroundColor = UIColor(hexString: "0AC775")
        
        toolbar.title = self.title
        toolbar.titleLabel.textColor = .white
        toolbar.titleLabel.textAlignment = .center
        toolbar.rightViews = [doneButton]
        toolbar.leftViews = [backButton]
    }
}

extension MWEditToolbarViewController {
    @objc
    fileprivate func backButtonClicked(button: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func doneButtonDidClicked(button: UIButton){
        if self.rootViewController.isKind(of: CreateBudgetViewController.self) {
            NotificationCenter.default.post(name: NSNotification.Name("savePeriodicBudget"), object: nil)
        }
        else if self.rootViewController.isKind(of: AddReminderViewController.self){
            NotificationCenter.default.post(name: NSNotification.Name("saveBillingReminder"), object: nil)
        }
        else {
            NotificationCenter.default.post(name: NSNotification.Name("saveTransaction"), object: nil)
        }

    }
}
