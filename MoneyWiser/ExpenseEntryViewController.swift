//
//  ExpenseEntryViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/31/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import FontAwesome
import Chameleon
import DLRadioButton
import RealmSwift
import SnapKit
import Timepiece

class ExpenseEntryViewController: UIViewController {

    fileprivate var amountField: TextField!
    fileprivate var categoryField: TextField!
    fileprivate var expenseTypeField: TextField!
    fileprivate var timestampField: TextField!
    fileprivate var firstChoice: DLRadioButton!
    fileprivate var secondChoice: DLRadioButton!
    fileprivate var expenseTypePicker: UIPickerView!
    
    let paymentOptions = ["Cash", "Credit Card", "Debit Card","Octopus", "Other"]
    let expenseOptions = ["Transportation", "Food", "Purchases", "Leisure", "Health", "Tuition","Other"]
    let incomeOptions = ["Pocket Money", "Salary", "Red Pocket", "Scholarship", "Other"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "New Transaction"
        prepareBalanceTypeSelection()
        prepareAmountField()
        prepareCategoryField()
        prepareTimestampField()
        prepareExpenseTypeField()
        registerNotifications()
        // Do any additional setup after loading the view.
    }

}

extension ExpenseEntryViewController {
    
    fileprivate func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(saveRecord), name: NSNotification.Name("saveTransaction"), object: nil)
    }
    
    fileprivate func prepareBalanceTypeSelection(){
        firstChoice = DLRadioButton(frame: CGRect(x: 0, y: 0, width: 96, height: 17))
        firstChoice.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        firstChoice.setTitle("Income", for: .normal)
        firstChoice.setTitleColor(MWStyles.themeColor, for: .normal)
        firstChoice.iconColor = MWStyles.themeColor
        firstChoice.indicatorColor = MWStyles.themeColor
        firstChoice.contentHorizontalAlignment = .left
        firstChoice.isSelected = true
        firstChoice.tag = 50
        firstChoice.addTarget(self, action: #selector(balanceTypeDidSelected), for: .touchUpInside)
        secondChoice = DLRadioButton(frame: CGRect(x: 0, y: 0, width: 96, height: 17))
        secondChoice.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        secondChoice.setTitle("Expense", for: .normal)
        secondChoice.setTitleColor(MWStyles.themeColor, for: .normal)
        secondChoice.iconColor = MWStyles.themeColor
        secondChoice.indicatorColor = MWStyles.themeColor
        secondChoice.contentHorizontalAlignment = .left
        secondChoice.tag = 51
        secondChoice.addTarget(self, action: #selector(balanceTypeDidSelected), for: .touchUpInside)
        view.addSubview(firstChoice)
        view.addSubview(secondChoice)
        firstChoice.snp.makeConstraints { (make) -> Void in
            make.width.equalToSuperview().multipliedBy(0.25)
            make.topMargin.equalToSuperview().offset(44)
            make.centerX.equalToSuperview().multipliedBy(0.8)
        }
        secondChoice.snp.makeConstraints { (make) -> Void in
            make.width.equalToSuperview().multipliedBy(0.25)
            make.centerY.equalTo(firstChoice)
            make.leftMargin.equalTo(firstChoice).offset(firstChoice.frame.width)
        }
    }
    
    fileprivate func prepareAmountField(){
        amountField = TextField()
        amountField.placeholder = "Amount"
        amountField.isClearIconButtonEnabled = true
        amountField.keyboardType = .decimalPad
        amountField.attachToolbar()
        amountField.delegate = self
        amountField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .money, textColor: .gray, size: CGSize(width: 22, height: 22)))
        amountField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        view.addSubview(amountField)
        amountField.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(firstChoice).offset(52)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareCategoryField(){
        categoryField = TextField()
        categoryField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        categoryField.placeholder = "Transaction Type"
        categoryField.isClearIconButtonEnabled = true
        categoryField.delegate = self
        categoryField.attachToolbar()
        categoryField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .creditCard, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(categoryField)
        categoryField.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(amountField).offset(64)
            make.centerX.equalToSuperview()
        }
        
    }
    
    fileprivate func prepareTimestampField(){
        timestampField = TextField()
        timestampField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        timestampField.placeholder = "Timestamp"
        timestampField.isClearIconButtonEnabled = true
        timestampField.delegate = self
        let timestampPicker = UIDatePicker()
        timestampPicker.datePickerMode = .dateAndTime
        timestampPicker.addTarget(self, action: #selector(timestampUpdated(sender:)), for: .valueChanged)
        timestampField.inputView = timestampPicker
        timestampField.attachToolbar()
        timestampField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .clockO, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(timestampField)
        timestampField.snp.makeConstraints{ (make) -> Void in
            make.topMargin.equalTo(categoryField).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareExpenseTypeField(){
        expenseTypeField = TextField()
        expenseTypeField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        expenseTypeField.placeholder = "Description"
        expenseTypeField.isClearIconButtonEnabled = true
        expenseTypeField.delegate = self
        expenseTypePicker = UIPickerView()
        expenseTypePicker.delegate = self
        expenseTypeField.inputView = expenseTypePicker
        expenseTypeField.attachToolbar()
        expenseTypeField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .questionCircle, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(expenseTypeField)
        expenseTypeField.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(timestampField).offset(64)
            make.centerX.equalToSuperview()
        }
    }
}

extension ExpenseEntryViewController {
    @objc fileprivate func balanceTypeDidSelected(button: DLRadioButton){
        switch button.tag {
        case 50:
            secondChoice.isSelected = !button.isSelected
        case 51:
            firstChoice.isSelected = !button.isSelected
        default:
            break
        }
    }
    
    @objc fileprivate func timestampUpdated(sender: UIDatePicker){
        print("passed here")
        let componenets = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year, let hour = componenets.hour, let minute = componenets.minute {
            timestampField.text = "\(day)-\(month)-\(year) \(hour):\(minute)"
        }
    }
    
    @objc fileprivate func saveRecord(){
        guard isInputValidated() else {
            let alertVc = UIAlertController(title: "Input cannot be empty", message: "Please check your inputs are valid", preferredStyle: .alert)
            alertVc.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertVc, animated: true, completion: nil)
            return
        }
        if let amount = amountField.text, let notes = expenseTypeField.text, let payment = categoryField.text, let timestamp = timestampField.text {
            let realm = try! Realm()
            let transaction = Transaction()
            transaction.amount = Double(amount)!
            transaction.date = timestamp.components(separatedBy: " ")[0].date(inFormat: "dd-MM-yyyy")!
            print(transaction.date)
            transaction.timestamp = timestamp.components(separatedBy: " ")[1]
            transaction.isExpense = secondChoice.isSelected
            transaction.reason = notes
            transaction.payment = payment
            if let target = realm.objects(Budget.self).last {
                transaction.trackingId = target.trackingId
                try! realm.write {
                    target.current = transaction.isExpense ? (target.current-transaction.amount) : (target.current+transaction.amount)
                }
            }
            try! realm.write {
                realm.add(transaction)
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name("transactionUpdated"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func isInputValidated() -> Bool {
        return !(amountField.text?.isEmpty)! && !(expenseTypeField.text?.isEmpty)! && !(categoryField.text?.isEmpty)!
    }
}

extension ExpenseEntryViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return firstChoice.isSelected ? incomeOptions.count : expenseOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return firstChoice.isSelected ? incomeOptions[row] : expenseOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if firstChoice.isSelected {
            expenseTypeField.text = incomeOptions[row]
        }
        else {
            expenseTypeField.text = expenseOptions[row]
        }
        return
    }
    
    
}

extension ExpenseEntryViewController : TextFieldDelegate {
    
}
