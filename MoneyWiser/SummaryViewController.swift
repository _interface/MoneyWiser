//
//  SummaryViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import Charts
import RealmSwift
import FontAwesome
import Chameleon
import Timepiece

class SummaryViewController: UIViewController {
    
    @IBOutlet weak var cashflowView: UIView!{
        didSet {
            cashflowView.backgroundColor = UIColor.init(gradientStyle: UIGradientStyle.topToBottom, withFrame: cashflowView.frame, andColors: [UIColor.init(hexString: "00c6ff"), UIColor.init(hexString: "0072ff")])
            cashflowView.layer.masksToBounds = false
            cashflowView.shadowColor = UIColor.black
            cashflowView.shadowOffset = CGSize(width: 0, height: 1.5)
            cashflowView.shadowOpacity = 0.24
            cashflowView.shadowRadius = CGFloat(1.5)
        }
    }

    @IBOutlet weak var expenseContributionView: UIView! {
        didSet {
            expenseContributionView.backgroundColor = UIColor.init(gradientStyle: UIGradientStyle.topToBottom, withFrame: expenseContributionView.frame, andColors: [UIColor.init(hexString: "92FE9D"), UIColor.init(hexString: "00C9FF")])
            expenseContributionView.layer.masksToBounds = false
            expenseContributionView.shadowColor = UIColor.black
            expenseContributionView.shadowOffset = CGSize(width: 0, height: 1.5)
            expenseContributionView.shadowOpacity = 0.24
            expenseContributionView.shadowRadius = CGFloat(1.5)
        }
    }
    @IBOutlet weak var weeklyStatsView: UIView! {
        didSet {
            weeklyStatsView.backgroundColor = UIColor.init(gradientStyle: UIGradientStyle.topToBottom, withFrame: weeklyStatsView.frame, andColors: [UIColor.init(hexString: "#F9D423"), UIColor.init(hexString: "#FF4E50")])
            weeklyStatsView.layer.masksToBounds = false
            weeklyStatsView.shadowColor = UIColor.black
            weeklyStatsView.shadowOffset = CGSize(width: 0, height: 1.5)
            weeklyStatsView.shadowOpacity = 0.24
            weeklyStatsView.shadowRadius = CGFloat(1.5)
        }
    }
    
    @IBOutlet weak var earnIcon: UILabel!{
        didSet {
            earnIcon.font = UIFont.fontAwesome(ofSize: 24)
            earnIcon.text = String.fontAwesomeIcon(name: .caretUp)
            earnIcon.textColor = UIColor.init(hexString: "0AC775")
        }
    }
    
    @IBOutlet weak var spentIcon: UILabel! {
        didSet {
            spentIcon.font = UIFont.fontAwesome(ofSize: 24)
            spentIcon.text = String.fontAwesomeIcon(name: .caretDown)
            spentIcon.textColor = UIColor.init(hexString: "F24965")
        }
    }
    
    @IBOutlet weak var balanceTextLabel: UILabel!
    @IBOutlet weak var incomeTextLabel: UILabel!
    @IBOutlet weak var expenseTextLabel: UILabel!
    
    @IBOutlet weak var expensePieChartView: UIView! {
        didSet {
            expensePieChartView.backgroundColor = .clear
        }
    }
    
    fileprivate var expensePieChart : PieChartView!
    fileprivate var weeklySpendingLineChart : LineChartView!
    fileprivate var weeklySpendings = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationItem()
        prepareWeeklySpendingLineChart()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareData()
    }
    
    override func viewWillLayoutSubviews() {
        expensePieChart.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        weeklySpendingLineChart.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
    }

}

extension SummaryViewController {
    
    fileprivate func prepareData(){
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        let realm = try! Realm()
        let balance = realm.objects(Budget.self).last
        guard let netBalance = balance else {
            return
        }
        if (netBalance.trackBegin...netBalance.trackEnd).contains(Date.today()) {
            balanceTextLabel.text = Helper.cleanDollars("\(netBalance.current)")
        }
        let netIncomes = realm.objects(Transaction.self).filter("trackingId == %@ AND isExpense == false", netBalance.trackingId).map{$0.amount}
        let netExpense = realm.objects(Transaction.self).filter("trackingId == %@ AND isExpense == true", netBalance.trackingId).map{$0.amount}
        incomeTextLabel.text = "$\(netIncomes.reduce(0, {$0 + $1}))\nIncome"
        expenseTextLabel.text = "$\(netExpense.reduce(0, {$0 + $1}))\nExpense"
        let transactions = realm.objects(Transaction.self)
        for index in 0...6 {
            print(calendar.startOfDay(for:(Date.today() - index.day)!))
            let dailySum = transactions.filter("date == %@", (calendar.startOfDay(for:(Date.today() - index.day)!)))
            weeklySpendings.append(dailySum.map{$0.amount}.reduce(0, {$0 + $1}))
        }
        let trackedSubjects = transactions.filter("date > %@ AND date < %@ AND isExpense == true", netBalance.trackBegin, netBalance.trackEnd)
        let expenses = Array(Set(trackedSubjects.map{ $0.reason }))
        var sumOfExpenses = [Double:String]()
        for expense in expenses {
            sumOfExpenses[trackedSubjects.filter("reason == %@", expense).map{$0.amount}.reduce(0, {$0 + $1})] = expense
        }
        let sortedArr = Array(sumOfExpenses).sorted{ $0.1 > $1.1 }
        var pieChartData = [Double]()
        var pieChartLabels = [String]()
        for pair in sortedArr {
            pieChartData.append(pair.key)
            pieChartLabels.append(pair.value)
        }
        prepareExpensePieChart(spendings: pieChartData, labels: pieChartLabels)
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.title = "Summary"
        navigationItem.detail = "Comprehensive Report"
        navigationItem.titleLabel.textColor = .white
        navigationItem.detailLabel.textColor = .white
        navigationController?.navigationBar.backgroundColor = MWStyles.themeColor
    }
}

extension SummaryViewController : ChartViewDelegate {
    internal func prepareExpensePieChart(spendings: [Double], labels: [String]) {
        expensePieChart = PieChartView(frame: CGRect(x: 0, y: 0, width: expensePieChartView.width*0.8, height: expensePieChartView.height*0.8))
        let colours = [UIColor.init(hexString: "2660A4"),UIColor.init(hexString: "FCCA46"),UIColor.init(hexString: "EA526F"),UIColor.init(hexString: "31D8A3"), UIColor.init(hexString: "F15152")]
        
        var entries = [PieChartDataEntry]()
        for (index, value) in spendings.enumerated() {
            let entry = PieChartDataEntry()
            entry.y = value
            entry.label = labels[index]
            entries.append( entry)
        }
        
        // 3. chart setup
        let set = PieChartDataSet( values: entries, label: "Pie Chart")
        // this is custom extension method. Download the code for more details.
        var colors: [UIColor] = []
        set.drawValuesEnabled = false
        set.selectionShift = 4
        
        for color in colours {
            colors.append(color!)
        }
        set.colors = colors
        let data = PieChartData(dataSet: set)
        expensePieChart.data = data
        expensePieChart.legend.enabled = false
        expensePieChart.noDataText = "No data available"
        expensePieChart.holeRadiusPercent = 0.45
        expensePieChart.drawEntryLabelsEnabled = true
        expensePieChart.entryLabelFont = UIFont.systemFont(ofSize: 8)
        expensePieChart.legend.drawInside = true
        expensePieChart.chartDescription?.enabled = false
//        let chartDescription = Description()
//        chartDescription.text = "Expense Attributions"
//        chartDescription.textColor = .white
//        expensePieChart.chartDescription = chartDescription
        expensePieChart.transparentCircleColor = UIColor.clear
        expensePieChart.holeColor = .clear
        expensePieChartView.addSubview(expensePieChart)
    }
    
    internal func prepareWeeklySpendingLineChart(){
        weeklySpendingLineChart = LineChartView(frame: CGRect(x: 0, y: 0, width: weeklyStatsView.frame.width, height: weeklyStatsView.frame.height*0.8))
        lineChartStyling()
        let expenses = [86.2, 43, 210, 121, 160, 65.3, 87]
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        for (index, value) in expenses.enumerated() {
            yVals1.append(ChartDataEntry(x: Double(index), y:value ))
        }
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(prepareLineChartDataset(data: yVals1))
        let data = LineChartData(dataSets: dataSets)
        weeklySpendingLineChart.data = data
        weeklyStatsView.addSubview(weeklySpendingLineChart)
    }
    
    internal func lineChartStyling(){
        weeklySpendingLineChart.chartDescription?.enabled = true
        let chartDescription = Description()
        chartDescription.text = "Weekly Expenditure Report"
        chartDescription.textColor = .white
        chartDescription.font = UIFont.systemFont(ofSize: 10)
        weeklySpendingLineChart.chartDescription = chartDescription
        weeklySpendingLineChart.legend.enabled = false
        weeklySpendingLineChart.pinchZoomEnabled = false
        weeklySpendingLineChart.backgroundColor = .clear
        weeklySpendingLineChart.grid.rows = 5
        weeklySpendingLineChart.leftAxis.enabled = false
        weeklySpendingLineChart.leftAxis.drawZeroLineEnabled = false
        weeklySpendingLineChart.xAxis.drawAxisLineEnabled = false
        weeklySpendingLineChart.xAxis.drawGridLinesEnabled = false
        weeklySpendingLineChart.xAxis.drawLabelsEnabled = false
        weeklySpendingLineChart.rightAxis.drawLabelsEnabled = false
        weeklySpendingLineChart.rightAxis.drawAxisLineEnabled = false
        weeklySpendingLineChart.rightAxis.gridColor = UIColor(white: 20, alpha: 0.25)
    }
    
    internal func prepareLineChartDataset(data: [ChartDataEntry]) -> LineChartDataSet {
        let lineChartDataset = LineChartDataSet(values: data, label: "")
        lineChartDataset.lineWidth = 2
        lineChartDataset.drawCirclesEnabled = true
        lineChartDataset.circleRadius = 4
        lineChartDataset.setColor(.white)
        lineChartDataset.setCircleColor(.white)
        lineChartDataset.drawValuesEnabled = true
        lineChartDataset.valueColors = [UIColor.white]
        lineChartDataset.valueFont = UIFont.systemFont(ofSize: 10)
        lineChartDataset.highlightLineWidth = 1.25
        lineChartDataset.drawHorizontalHighlightIndicatorEnabled = false
        lineChartDataset.highlightColor = UIColor(white: 255, alpha: 0.5)
        lineChartDataset.fillAlpha = 1
        let colours = [UIColor.white.withAlphaComponent(0).cgColor, UIColor.white.cgColor] as CFArray
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: colorSpace, colors: colours , locations: nil)
        lineChartDataset.fill = Fill(linearGradient: gradient!, angle: 90)
        lineChartDataset.drawFilledEnabled = true
        return lineChartDataset
    }
}

class ChartXAxisFormatter: NSObject {
    fileprivate var dateFormatter: DateFormatter?
    fileprivate var referenceTimeInterval: TimeInterval?
    
    convenience init(referenceTimeInterval: TimeInterval, dateFormatter: DateFormatter) {
        self.init()
        self.referenceTimeInterval = referenceTimeInterval
        self.dateFormatter = dateFormatter
    }
}


extension ChartXAxisFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        guard let dateFormatter = dateFormatter,
            let referenceTimeInterval = referenceTimeInterval
            else {
                return ""
        }
        
        let date = Date(timeIntervalSince1970: value * 3600 * 24 + referenceTimeInterval)
        return dateFormatter.string(from: date)
    }
    
}
