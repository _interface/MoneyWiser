//
//  AddReminderViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 4/2/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import FontAwesome
import RealmSwift
import UserNotifications

class AddReminderViewController: UIViewController {
    
    fileprivate var billingAmount : TextField!
    fileprivate var billingCategory : TextField!
    fileprivate var billingDueDate : TextField!
    fileprivate var daysUntilNextDue : TextField!
    fileprivate let billingCategories = ["Tuition", "Phone Services", "Others"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Reminder"
        prepareBillingAmount()
        prepareBillingCategory()
        prepareDueDate()
        prepapreDaysUntilNextDue()
        registerNotifications()
        // Do any additional setup after loading the view.
    }
    
    

}

extension AddReminderViewController {
    
    fileprivate func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(saveReminder), name: NSNotification.Name("saveBillingReminder"), object: nil)
    }
    
  
    fileprivate func prepareBillingAmount(){
        billingAmount = TextField()
        billingAmount.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        billingAmount.placeholder = "Amount"
        billingAmount.isClearIconButtonEnabled = true
        billingAmount.keyboardType = .decimalPad
        billingAmount.attachToolbar()
        billingAmount.delegate = self
        billingAmount.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .dollar, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(billingAmount)
        billingAmount.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalToSuperview().offset(32)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareBillingCategory(){
        billingCategory = TextField()
        billingCategory.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        billingCategory.placeholder = "Category"
        billingCategory.isClearIconButtonEnabled = false
        billingCategory.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .tag, textColor: .gray, size: CGSize(width: 22, height: 22)))
        let billingCategoryPicker = UIPickerView()
        billingCategoryPicker.delegate = self
        billingCategory.inputView = billingCategoryPicker
        billingCategory.attachToolbar()
        view.addSubview(billingCategory)
        billingCategory.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(billingAmount).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareDueDate(){
        billingDueDate = TextField()
        billingDueDate.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        billingDueDate.placeholder = "Due Date"
        billingDueDate.isClearIconButtonEnabled = false
        billingDueDate.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .calendarCheckO, textColor: .gray, size: CGSize(width: 22, height: 22)))
        let dueDatePicker = UIDatePicker()
        dueDatePicker.datePickerMode = .date
        dueDatePicker.addTarget(self, action: #selector(dueDateUpdated), for: .valueChanged)
        billingDueDate.inputView = dueDatePicker
        billingDueDate.attachToolbar()
        view.addSubview(billingDueDate)
        billingDueDate.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(billingCategory).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepapreDaysUntilNextDue(){
        daysUntilNextDue = TextField()
        daysUntilNextDue.keyboardType = .numberPad
        daysUntilNextDue.attachToolbar()
        daysUntilNextDue.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        daysUntilNextDue.placeholder = "Days for recurring bill"
        daysUntilNextDue.isClearIconButtonEnabled = false
        daysUntilNextDue.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .calendarMinusO, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(daysUntilNextDue)
        daysUntilNextDue.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(billingDueDate).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    @objc fileprivate func dueDateUpdated(sender: UIDatePicker){
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            billingDueDate.text = "\(day)-\(month)-\(year)"
        }
    }
    
    @objc fileprivate func saveReminder(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if let amount = billingAmount.text, let category = billingCategory.text, let dueDate = billingDueDate.text, let next = daysUntilNextDue.text {
            let reminder = Reminder()
            reminder.identifier = UUID().uuidString
            reminder.amount = Double(amount)!
            reminder.category = category
            reminder.due = dateFormatter.date(from: dueDate)!
            reminder.next = Int(next)!
            let realm = try! Realm()
            try! realm.write {
                realm.add(reminder)
            }
            let notificationContent = UNMutableNotificationContent()
            notificationContent.title = "Billing Reminder : \(category)"
            notificationContent.body = "You have a reminder for the bill in \(category) to pay \(amount) today"
            notificationContent.sound = .default()
            let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: dateFormatter.date(from: dueDate)!),
                                                        repeats: false)
            let notificationRequest = UNNotificationRequest(identifier: reminder.identifier, content: notificationContent, trigger: trigger)
            UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: { (error) in
                if let error = error {
                    print(error)
                }
            })
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddReminderViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return billingCategories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return billingCategories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        billingCategory.text = billingCategories[row]
    }
}

extension AddReminderViewController : TextFieldDelegate {
    
}
