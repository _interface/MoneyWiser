//
//  SplashViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Chameleon

extension UILabel{
    func addTextSpacing(_ spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: text!)
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSRange(location: 0, length: text!.characters.count))
        attributedText = attributedString
    }
}

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.init(gradientStyle: UIGradientStyle.topToBottom, withFrame: self.view.frame, andColors: [UIColor.init(hexString: "#DAE2F8"), UIColor.init(hexString: "#D6A4A4")])
        
        let appUpperTitleTextLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 0.7*view.frame.size.width/2, y: view.frame.size.height/2 - 0.1*view.frame.size.height/2, width: 0.7*view.frame.size.width, height: 0.1*view.frame.size.height))
        appUpperTitleTextLabel.text = "MONEY"
        appUpperTitleTextLabel.textAlignment = .center
        appUpperTitleTextLabel.textColor = UIColor.white
        //        appTitleTextLabel.backgroundColor = UIColor.brown
        appUpperTitleTextLabel.font = UIFont(name: "AvenirNext-Regular", size: 20)
        appUpperTitleTextLabel.addTextSpacing(18)
        let appLowerTitleTextLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 0.7*view.frame.size.width/2, y: appUpperTitleTextLabel.frame.minY+30, width: 0.7*view.frame.size.width, height: 0.1*view.frame.size.height))
        appLowerTitleTextLabel.text = "WISER"
        appLowerTitleTextLabel.textAlignment = .center
        appLowerTitleTextLabel.textColor = UIColor.white
        //        appTitleTextLabel.backgroundColor = UIColor.brown
        appLowerTitleTextLabel.font = UIFont(name: "AvenirNext-Regular", size: 18)
        appLowerTitleTextLabel.addTextSpacing(18)
        view.addSubview(appUpperTitleTextLabel)
        view.addSubview(appLowerTitleTextLabel)
        // Do any additional setup after loading the view.
    }

}
