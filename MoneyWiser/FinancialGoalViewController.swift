//
//  FinancialGoalViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 4/12/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit

class FinancialGoalViewController: UIViewController {

    @IBOutlet weak var financialGoalProgressView: LinearProgressView! {
        didSet {
            financialGoalProgressView.trackColor = UIColor.flatWhiteColorDark()
            financialGoalProgressView.barColor = UIColor.flatGreen()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem(title: "Goals")
    }

}

extension FinancialGoalViewController {
    internal func setupViews(){
        financialGoalProgressView.progressValue = CGFloat(40)
    }
    
    internal func preparePageTabBarItem(title: String){
        pageTabBarItem.title = title
        pageTabBarItem.titleColor = .white
    }
}
