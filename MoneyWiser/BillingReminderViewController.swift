//
//  BillingReminderViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/31/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import RealmSwift

class BillingReminderViewController: UIViewController {

    @IBOutlet weak var reminderTableView: UITableView! {
        didSet{
            reminderTableView.separatorStyle = .none
        }
    }
    
    fileprivate var billingTypes = [String]()
    fileprivate var billingAmounts = [String]()
    fileprivate var remainingDays = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareRemindersData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem(title: "Reminders")
    }
    
}

extension BillingReminderViewController {
    fileprivate func prepareRemindersData(){
        let realm = try! Realm()
        let reminders = realm.objects(Reminder.self).filter("expired == false")
        billingTypes = reminders.map{ $0.category }
        billingAmounts = reminders.map { Helper.cleanDollars("\($0.amount)") }
        print(reminders)
        remainingDays = reminders.map{ $0.due.interval(ofComponent: .day, fromDate: Date.today())}
        reminderTableView.reloadData()
        
    }
    
    internal func preparePageTabBarItem(title: String){
        pageTabBarItem.title = title
        pageTabBarItem.titleColor = .white
    }
    
    @objc fileprivate func checkButtonDidClick(){
        let actionsheet = UIAlertController(title: "Choose an action", message: "", preferredStyle: .actionSheet)
        actionsheet.addAction(UIAlertAction(title: "Mark as paid", style: .default, handler: nil))
        actionsheet.addAction(UIAlertAction(title: "Remove reminder", style: .destructive, handler: nil))
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionsheet, animated: true, completion: nil)
    }
}

extension BillingReminderViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billingTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = reminderTableView.dequeueReusableCell(withIdentifier: "reminderCellIdentifier") else {
            return UITableViewCell()
        }
        switch billingTypes[indexPath.row] {
        case "Tuition":
            (cell.viewWithTag(1) as! UIImageView).image = UIImage.fontAwesomeIcon(name: .graduationCap, textColor: UIColor.flatBlue(), size: CGSize(width: 22, height: 22))
        case "Phone Services":
            (cell.viewWithTag(1) as! UIImageView).image = UIImage.fontAwesomeIcon(name: .phone, textColor: UIColor.flatForestGreen(), size: CGSize(width: 22, height: 22))
        default:
            (cell.viewWithTag(1) as! UIImageView).image = UIImage.fontAwesomeIcon(name: .tags, textColor: UIColor.flatPlum(), size: CGSize(width: 22, height: 22))
        }
        (cell.viewWithTag(3) as! UILabel).text = "\(billingTypes[indexPath.row]) - \(billingAmounts[indexPath.row])"
        (cell.viewWithTag(4) as! UILabel).font = UIFont.fontAwesome(ofSize: 14)
        (cell.viewWithTag(4) as! UILabel).text = String.fontAwesomeIcon(name: .clockO)
        (cell.viewWithTag(5) as! UIButton).titleLabel?.font = UIFont.fontAwesome(ofSize: 22)
        (cell.viewWithTag(5) as! UIButton).tintColor = UIColor.flatBlueColorDark()
        (cell.viewWithTag(5) as! UIButton).addTarget(self, action: #selector(checkButtonDidClick), for: .touchUpInside)
        (cell.viewWithTag(5) as! UIButton).setTitle(String.fontAwesomeIcon(name: .checkSquareO), for: .normal)
        (cell.viewWithTag(6))?.isHidden = (indexPath.row == 3)
        print(remainingDays[indexPath.row])
        if remainingDays[indexPath.row] <= 0 {
            (cell.viewWithTag(4) as! UILabel).text = String.fontAwesomeIcon(name: .warning)
            (cell.viewWithTag(4) as! UILabel).textColor = UIColor.flatYellow()
            (cell.viewWithTag(7) as! UILabel).text = "Overdue"
        }
        else {
            (cell.viewWithTag(7) as! UILabel).text = "Due in \(remainingDays[indexPath.row]) days"
        }
        
        return cell
    }
}
