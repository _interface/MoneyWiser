//
//  MWPageTabViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/30/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material

class MWPageTabViewController: PageTabBarController {

    open override func prepare() {
        super.prepare()
        delegate = self
        preparePageTabBar()
        prepareNavigationItem()
    }

}

extension MWPageTabViewController {
    fileprivate func preparePageTabBar() {
        pageTabBarAlignment = .top
        pageTabBar.dividerColor = nil
        pageTabBar.lineColor = .white
        pageTabBar.lineAlignment = .bottom
        pageTabBar.backgroundColor = MWStyles.themeColor
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.title = "Dashboard"
        navigationItem.titleLabel.textColor = .white
        navigationController?.navigationBar.backgroundColor = MWStyles.themeColor
    }
}

extension MWPageTabViewController : PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
        print("transit to")
    }
}
