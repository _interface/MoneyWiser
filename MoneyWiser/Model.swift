//
//  Model.swift
//  MoneyWiser
//
//  Created by Kristopher on 4/1/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import Foundation
import RealmSwift

class Transaction : Object {
    dynamic var amount : Double = 0
    dynamic var isExpense : Bool = false
    dynamic var payment : String = ""
    dynamic var reason : String = ""
    dynamic var date : Date = Date()
    dynamic var timestamp : String = ""
    dynamic var trackingId : String = ""
}

class Budget : Object {
    dynamic var limit : Double = 0
    dynamic var current : Double = 0
    dynamic var target : Double = 0
    dynamic var trackBegin : Date = Date()
    dynamic var trackEnd : Date = Date()
    dynamic var trackingId : String = ""
}

class Reminder : Object {
    dynamic var identifier: String = ""
    dynamic var due : Date = Date()
    dynamic var amount: Double = 0
    dynamic var category: String = ""
    dynamic var next : Int = 0
    dynamic var expired : Bool = false
}
