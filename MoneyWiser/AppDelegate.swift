//
//  AppDelegate.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import ESTabBarController
import SwiftyUserDefaults
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabbarController : ESTabBarController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        prepareTabbar()
        stylingTabbar()
        if let window = window {
            window.backgroundColor = UIColor.white
            window.rootViewController = tabbarController
            window.makeKeyAndVisible()
        }
        initializeUserPreferences()
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if !granted {
                
            }
            // Enable or disable features based on authorization.
        }
        return true
    }
    
    internal func prepareTabbar(){
        tabbarController = ESTabBarController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let expenditureVC = ExpenditureViewController()
        let toolbarVC = MWToolbarViewController(rootViewController: expenditureVC)
        let homeController = MWMenuViewController(rootViewController: toolbarVC)
        homeController.tabBarItem = ESTabBarItem.init(title: nil, image: MWStyles.imageOfHomeicon(isSelected: false), selectedImage: MWStyles.imageOfHomeicon(isSelected: true), tag: 1)
        let summaryVC = storyboard.instantiateViewController(withIdentifier: "summaryView") as! SummaryViewController
        let summaryNavVC = MWNavigationController(rootViewController: summaryVC)
        summaryNavVC.tabBarItem = ESTabBarItem.init(title: nil, image: MWStyles.imageOfStatisticsicon(isSelected: false), selectedImage: MWStyles.imageOfStatisticsicon(isSelected: true), tag: 2)
        let reminderVC = storyboard.instantiateViewController(withIdentifier: "remindersView") as! BillingReminderViewController
        let finGoalVC = storyboard.instantiateViewController(withIdentifier: "financialGoalsView") as! FinancialGoalViewController
        let pageTabVC = MWPageTabViewController(viewControllers: [reminderVC, finGoalVC])
        let dashboardVC = MWNavigationController(rootViewController: pageTabVC)
        dashboardVC.tabBarItem = ESTabBarItem.init(title: nil, image: MWStyles.imageOfRemindericon(isSelected: false), selectedImage: MWStyles.imageOfRemindericon(isSelected: true), tag: 3)
        let settingsVC = storyboard.instantiateViewController(withIdentifier: "settingsView") as! SettingsViewController
        let settingsNavVC = MWNavigationController(rootViewController: settingsVC)
        settingsNavVC.tabBarItem = ESTabBarItem.init(title: nil, image: MWStyles.imageOfSettingsicon(isSelected: false), selectedImage: MWStyles.imageOfSettingsicon(isSelected: true), tag: 4)
        tabbarController?.viewControllers = [homeController, summaryNavVC, dashboardVC, settingsNavVC]
    }

    internal func stylingTabbar(){
        if let tabBar = tabbarController?.tabBar as? ESTabBar {
            tabBar.backgroundColor = .white
            for item in tabBar.items! {
                (item as! ESTabBarItem).contentView?.renderingMode = .automatic
            }
        }
    }
    
    internal func initializeUserPreferences(){
        guard Defaults[.displayCurrency] == nil && Defaults[.languagePreference] == nil && Defaults[.enableNotifications] == nil else {
            return
        }
        Defaults[.displayCurrency] = "HKD"
        Defaults[.languagePreference] = "English"
        Defaults[.enableNotifications] = true
    }
}

