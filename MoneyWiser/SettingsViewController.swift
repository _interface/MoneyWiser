//
//  SettingsViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import FontAwesome
import SwiftyUserDefaults
import Localize_Swift

struct Preferences {
    let language : String
    let currency : String
    let isNotificationsEnabled : Bool
}

class SettingsViewController: UIViewController {
    
    fileprivate var applicationSettingsTitle: [String] = ["Currency", "Notifications", "Language", "Data Export"]
    fileprivate var applicationSettingsIcons: [FontAwesome] = [.dollar, .bullhorn, .language, .signOut]
    fileprivate var miscellaneousPreferenceTitles: [String] = ["Security", "Device Sync"]
    fileprivate var miscellaneousPreferenceIcons: [FontAwesome] = [.lock, .refresh]
    fileprivate var currentProfile : Preferences!
    
    @IBOutlet weak var settingsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSettingsTableView()
        prepareTableHeaderView()
        prepareNavigationItem()
        prepareSavedPreferences()
        // Do any additional setup after loading the view.
    }

}


class SettingsSectionHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
}
extension SettingsViewController {
    
    fileprivate func prepareSavedPreferences(){
        currentProfile = Preferences(language: Defaults[.languagePreference]!, currency: Defaults[.displayCurrency]!, isNotificationsEnabled: Defaults[.enableNotifications]!)
    }
    
    fileprivate func prepareTableHeaderView(){
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        settingsTableView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.title = "Settings"
        navigationItem.titleLabel.textColor = .white
        navigationController?.navigationBar.backgroundColor = MWStyles.themeColor
    }
    
    fileprivate func prepareSettingsTableView(){
        settingsTableView.separatorStyle = .none
    }
    
    fileprivate func toggleNotifications(){
        let actionSheet = UIAlertController(title: "Enable Notifications", message: "Attention: Disabling Notifications will remove all scheduled notifications for Billing Reminder", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: currentProfile.isNotificationsEnabled ? "Disable" : "Enable" , style: .default, handler: { action -> Void in
            Defaults[.enableNotifications] = !self.currentProfile.isNotificationsEnabled
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    fileprivate func selectDisplayLanguage(){
        let actionSheet = UIAlertController(title: "Choose the display language from below", message: "Current: \(currentProfile.language)", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "English", style: .default, handler: { action -> Void in
            Defaults[.languagePreference] = "English"
        }))
        actionSheet.addAction(UIAlertAction(title: "繁體中文", style: .default, handler: { action -> Void in
            Defaults[.languagePreference] = "繁體中文"
        }))
        actionSheet.addAction(UIAlertAction(title: "简体中文", style: .default, handler: { action -> Void in
            Defaults[.languagePreference] = "简体中文"
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: { handler -> Void in
            self.prepareSavedPreferences()
            self.settingsTableView.reloadData()
        })
    }
}

extension SettingsViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        default:
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = settingsTableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader")
        let header = cell as! SettingsSectionHeader
        switch section {
        case 1:
            header.titleLabel.text = "MISCELLANEOUS"
        default:
            header.titleLabel.text = "GENERAL"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "General"
        default:
            return "Miscellanous"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsViewCell", for: indexPath)
//        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let iconColor : UIColor! = UIColor.init(hexString: "67D4C9")
        let titleColor : UIColor! = UIColor.init(hexString: "95A3B3")
        (cell.viewWithTag(2) as! UILabel).textColor = titleColor
        (cell.viewWithTag(3) as! UILabel).textColor = titleColor
        switch indexPath.section {
        case 1:
            (cell.viewWithTag(1) as! UIImageView).image = UIImage.fontAwesomeIcon(name: miscellaneousPreferenceIcons[indexPath.row], textColor: iconColor, size: CGSize(width: 22, height: 22))
            (cell.viewWithTag(2) as! UILabel).text = miscellaneousPreferenceTitles[indexPath.row]
            switch indexPath.row {
            case 0:
                (cell.viewWithTag(3) as! UILabel).text = "Password"
            default:
                (cell.viewWithTag(3) as! UILabel).font = UIFont.fontAwesome(ofSize: 22)
                (cell.viewWithTag(3) as! UILabel).text = String.fontAwesomeIcon(name: .angleRight)
            }
        default:
            (cell.viewWithTag(1) as! UIImageView).image = UIImage.fontAwesomeIcon(name: applicationSettingsIcons[indexPath.row], textColor: iconColor, size: CGSize(width: 22, height: 22))
            (cell.viewWithTag(2) as! UILabel).text = applicationSettingsTitle[indexPath.row]
            switch indexPath.row {
            case 0:
                (cell.viewWithTag(3) as! UILabel).text = currentProfile.currency
            case 1:
                (cell.viewWithTag(3) as! UILabel).text = currentProfile.isNotificationsEnabled ? "ON" : "OFF"
            case 2:
                (cell.viewWithTag(3) as! UILabel).text = currentProfile.language
            default:
                (cell.viewWithTag(3) as! UILabel).font = UIFont.fontAwesome(ofSize: 22)
                (cell.viewWithTag(3) as! UILabel).text = String.fontAwesomeIcon(name: .angleRight)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 1:
            switch indexPath.row {
            case 2:
                print("selected")
            default:
                print("selected")
            }
        default:
            switch indexPath.row {
            case 1:
                toggleNotifications()
            case 2:
                selectDisplayLanguage()
            case 3:
                print("selected")
            default:
                print("selected")
            }
        }
    }
}
