//
//  CreateBudgetViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 8/5/2017.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Chameleon
import FontAwesome
import Material
import SnapKit
import RealmSwift
import Timepiece

class CreateBudgetViewController: UIViewController {
    
    fileprivate var budgetField : TextField!
    fileprivate var targetField : TextField!
    fileprivate var periodStart : TextField!
    fileprivate var periodEnd : TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add Budget"
        prepareBudgetTextfield()
        prepareTargetField()
        prepareDateBeginEntry()
        prepareDateEndEntry()
        registerNotifications()
        // Do any additional setup after loading the view.
    }
    
}

extension CreateBudgetViewController {
    
    fileprivate func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(saveCurrentBudget), name: NSNotification.Name("savePeriodicBudget"), object: nil)
    }
    
    fileprivate func prepareBudgetTextfield() {
        budgetField = TextField()
        budgetField.placeholder = "Budget Amount"
        budgetField.isClearIconButtonEnabled = true
        budgetField.keyboardType = .decimalPad
        budgetField.attachToolbar()
        budgetField.delegate = self
        budgetField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .dollar, textColor: .gray, size: CGSize(width: 22, height: 22)))
        budgetField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        view.addSubview(budgetField)
        budgetField.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalToSuperview().offset(32)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareTargetField(){
        targetField = TextField()
        targetField.placeholder = "Target Spending Limit"
        targetField.isClearIconButtonEnabled = true
        targetField.keyboardType = .decimalPad
        targetField.attachToolbar()
        targetField.delegate = self
        targetField.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .flag, textColor: .gray, size: CGSize(width: 22, height: 22)))
        targetField.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        view.addSubview(targetField)
        targetField.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(budgetField).offset(64)
            make.centerX.equalToSuperview()
        }
        
    }
    
    fileprivate func prepareDateBeginEntry() {
        periodStart = TextField()
        periodStart.placeholder = "Tracking Begin"
        let periodDatePicker = UIDatePicker()
        periodDatePicker.datePickerMode = .date
        periodDatePicker.tag = 1
        periodDatePicker.addTarget(self, action: #selector(dateDidUpdated(sender:)), for: .valueChanged)
        periodStart.inputView = periodDatePicker
        periodStart.attachToolbar()
        periodStart.isClearIconButtonEnabled = false
        periodStart.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .calendar, textColor: .gray, size: CGSize(width: 22, height: 22)))
        periodStart.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        view.addSubview(periodStart)
        periodStart.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(targetField).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    fileprivate func prepareDateEndEntry(){
        periodEnd = TextField()
        periodEnd.placeholder = "Tracking End"
        let periodDatePicker = UIDatePicker()
        periodDatePicker.datePickerMode = .date
        periodDatePicker.tag = 2
        periodDatePicker.addTarget(self, action: #selector(dateDidUpdated(sender:)), for: .valueChanged)
        periodEnd.attachToolbar()
        periodEnd.inputView = periodDatePicker
        periodEnd.isClearIconButtonEnabled = false
        periodEnd.frame = CGRect(x: 0, y: 0, width: view.frame.width*0.9, height: view.frame.height*0.08)
        periodEnd.leftView = UIImageView(image: UIImage.fontAwesomeIcon(name: .calendar, textColor: .gray, size: CGSize(width: 22, height: 22)))
        view.addSubview(periodEnd)
        periodEnd.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(periodStart).offset(64)
            make.centerX.equalToSuperview()
        }
    }
    
    @objc fileprivate func dateDidUpdated(sender: UIDatePicker){
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            switch sender.tag {
            case 2:
                periodEnd.text = "\(day)-\(month)-\(year)"
            default:
                periodStart.text = "\(day)-\(month)-\(year)"
            }
        }
    }
    
    @objc fileprivate func saveCurrentBudget(){
        print("passed here")
        if let amount = budgetField.text,let target = targetField.text, let dateBegin = periodStart.text, let dateEnd = periodEnd.text {
            let budget = Budget()
            budget.limit = Double(amount)!
            budget.current = Double(amount)!
            budget.target = Double(target)!
            budget.trackBegin = dateBegin.date(inFormat: "dd-MM-yyyy")!
            budget.trackEnd = dateEnd.date(inFormat: "dd-MM-yyyy")!
            budget.trackingId = UUID().uuidString
            let realm = try! Realm()
            try! realm.write {
                realm.add(budget)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension CreateBudgetViewController : TextFieldDelegate {
}
