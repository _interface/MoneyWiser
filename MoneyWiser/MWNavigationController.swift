//
//  MWNavigationControllerController.swift
//  MoneyWiser
//
//  Created by Kristopher on 4/3/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material

class MWNavigationController: NavigationController {

    open override func prepare() {
        super.prepare()
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        self.statusBarController?.statusBar.backgroundColor = MWStyles.themeColor
        self.navigationBar.backgroundColor = MWStyles.themeColor
        v.depthPreset = .none
//        v.dividerColor = Color.grey.lighten3
    }

}
