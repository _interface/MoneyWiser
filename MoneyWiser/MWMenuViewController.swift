//
//  MWMenuViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material
import Chameleon
import FontAwesome

class MWMenuViewController: FABMenuController {
    fileprivate let fabMenuSize = CGSize(width: 56, height: 56)
    fileprivate let bottomInset: CGFloat = 24
    fileprivate let rightInset: CGFloat = 24
    
    fileprivate var fabButton: FABButton!
    fileprivate var evMenuItem: FABMenuItem!
    fileprivate var svMenuItem: FABMenuItem!
    fileprivate var bgdtMenuItem: FABMenuItem!

    open override func prepare() {
        super.prepare()
        view.backgroundColor = .white
        
        prepareFABButton()
        prepareEvFABMenuItem()
        prepareSvFABMenuItem()
        prepareBudgetButton()
        prepareFABMenu()
        
    }

}

extension MWMenuViewController {
    fileprivate func prepareFABButton() {
        fabButton = FABButton(image: Icon.cm.menu, tintColor: .white)
        fabButton.pulseColor = .white
        fabButton.backgroundColor = MWStyles.themeColor
    }
    
    fileprivate func prepareEvFABMenuItem(){
        evMenuItem = FABMenuItem()
        evMenuItem.title = "Add Billing Reminder"
        evMenuItem.fabButton.image = UIImage.fontAwesomeIcon(name: .bellO, textColor: .white, size: CGSize(width: 22, height: 22))
        evMenuItem.fabButton.tintColor = .white
        evMenuItem.fabButton.pulseColor = .white
        evMenuItem.fabButton.backgroundColor = MWStyles.themeColor
        evMenuItem.fabButton.addTarget(self, action: #selector(addReminderButtonDidClick), for: .touchUpInside)
    }
    
    fileprivate func prepareBudgetButton(){
        bgdtMenuItem = FABMenuItem()
        bgdtMenuItem.title = "Create New Budget"
        bgdtMenuItem.fabButton.image = UIImage.fontAwesomeIcon(name: .sliders, textColor: .white, size: CGSize(width: 22, height: 22))
        bgdtMenuItem.fabButton.tintColor = .white
        bgdtMenuItem.fabButton.pulseColor = .white
        bgdtMenuItem.fabButton.backgroundColor = MWStyles.themeColor
        bgdtMenuItem.fabButton.addTarget(self, action: #selector(addBudgetButtonDidClick), for: .touchUpInside)
    }
    
    fileprivate func prepareSvFABMenuItem(){
        svMenuItem = FABMenuItem()
        svMenuItem.title = "Transaction Entry"
        svMenuItem.fabButton.image = UIImage.fontAwesomeIcon(name: .calculator, textColor: .white, size: CGSize(width: 22, height: 22))
        svMenuItem.fabButton.tintColor = .white
        svMenuItem.fabButton.pulseColor = .white
        svMenuItem.fabButton.backgroundColor = UIColor.init(hexString: "0AC775")
        svMenuItem.fabButton.addTarget(self, action: #selector(addRecordButtonDidClick), for: .touchUpInside)
    }
    
    fileprivate func prepareFABMenu(){
        fabMenu.fabButton = fabButton
        fabMenu.fabMenuItems = [bgdtMenuItem ,svMenuItem ,evMenuItem]
        
        view.layout(fabMenu)
            .size(fabMenuSize)
            .bottom(bottomInset+36)
            .right(rightInset)
    }
}

extension MWMenuViewController {
    @objc
    fileprivate func addRecordButtonDidClick(button: UIButton){
        let toolbarVC = MWEditToolbarViewController(rootViewController: ExpenseEntryViewController())
        self.present(toolbarVC, animated: true) { 
            self.fabMenu.close()
        }
    }
    @objc
    fileprivate func addBudgetButtonDidClick(button: UIButton){
        let toolbarVC = MWEditToolbarViewController(rootViewController: CreateBudgetViewController())
        self.present(toolbarVC, animated: true){
            self.fabMenu.close()
        }
    }
    @objc
    fileprivate func addReminderButtonDidClick(button: UIButton){
        let toolbarVC = MWEditToolbarViewController(rootViewController: AddReminderViewController())
        self.present(toolbarVC, animated: true){
            self.fabMenu.close()
        }
    }
}

extension MWMenuViewController {
    @objc
    open func fabMenuWillOpen(fabMenu: FABMenu) {
        //        fabMenu.fabButton?.animate(animation: Motion.rotate(angle: 45))
        
        print("fabMenuWillOpen")
    }
    
    @objc
    open func fabMenuDidOpen(fabMenu: FABMenu) {
        print("fabMenuDidOpen")
    }
    
    @objc
    open func fabMenuWillClose(fabMenu: FABMenu) {
        //        fabMenu.fabButton?.animate(animation: Motion.rotate(angle: 0))
        
        print("fabMenuWillClose")
    }
    
    @objc
    open func fabMenuDidClose(fabMenu: FABMenu) {
        print("fabMenuDidClose")
    }
    
    @objc
    open func fabMenu(fabMenu: FABMenu, tappedAt point: CGPoint, isOutside: Bool) {
        print("fabMenuTappedAtPointIsOutside", point, isOutside)
        
        guard isOutside else {
            return
        }
        
        // Do something ...
    }
}
