//
//  MWNavigationViewController.swift
//  MoneyWiser
//
//  Created by Kristopher on 3/29/17.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import UIKit
import Material

class MWToolbarViewController: ToolbarController {
    
    fileprivate var menuButton: IconButton!
    fileprivate var reminderButton: IconButton!
    fileprivate var settingsButton: IconButton!
    
    open override func prepare() {
        super.prepare()
        prepareMenuButton()
        prepareReminderButton()
        prepareSettingsButton()
        prepareStatusBar()
        prepareToolbar()
    }
}

extension MWToolbarViewController {
    fileprivate func prepareMenuButton() {
        menuButton = IconButton(image: UIImage.fontAwesomeIcon(name: .tasks, textColor: .white, size: CGSize(width: 22, height: 22)), tintColor: .white)
        menuButton.pulseColor = .white
    }
    
    fileprivate func prepareReminderButton() {
        reminderButton = IconButton(image: Icon.cm.bell, tintColor: .white)
        reminderButton.pulseColor = .white
        reminderButton.addTarget(self, action: #selector(reminderButtonDidClicked), for: .touchUpInside)
    }
    
    fileprivate func prepareSettingsButton() {
        settingsButton = IconButton(image: Icon.cm.settings, tintColor: .white)
        settingsButton.pulseColor = .white
        settingsButton.addTarget(self, action: #selector(settingsButtonClicked), for: .touchUpInside)
    }
    
    fileprivate func prepareStatusBar(){
        statusBarStyle = .lightContent
        statusBar.backgroundColor = UIColor(hexString: "0AC775")
    }
    
    fileprivate func prepareToolbar() {
        
        toolbar.depthPreset = .none
        toolbar.backgroundColor = UIColor(hexString: "0AC775")
        
        toolbar.title = "Overview"
        toolbar.titleLabel.textColor = .white
        toolbar.titleLabel.textAlignment = .left
        
        toolbar.detailLabel.textColor = .white
        toolbar.detailLabel.textAlignment = .left
        toolbar.detail = "Monthly Expenditure"
        toolbar.leftViews = [menuButton]
    }
}

extension MWToolbarViewController : UIActionSheetDelegate{
    @objc
    fileprivate func settingsButtonClicked(button: UIButton) {
    }
    
    @objc
    fileprivate func reminderButtonDidClicked(button: UIButton){
    }
}
