//
//  Helper.swift
//  MoneyWiser
//
//  Created by Kristopher on 10/5/2017.
//  Copyright © 2017 Playground Canvas. All rights reserved.
//

import Foundation
import UIKit
import Material


extension TextField {
    func attachToolbar() {
        let pickerToolbar = UIToolbar()
        pickerToolbar.barStyle = .default
        pickerToolbar.tintColor = MWStyles.themeColor
        pickerToolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.resignFirstResponder))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.clearAndDismiss))
        pickerToolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        pickerToolbar.isUserInteractionEnabled = true
        self.inputAccessoryView = pickerToolbar
    }
    
    func clearAndDismiss(){
        self.text = ""
        self.resignFirstResponder()
    }
}

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}

class Helper {
    static func cleanDollars(_ value: String?) -> String {
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let formatter = NumberFormatter()
        formatter.currencyCode = "USD"
        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = (value!.contains(".00")) ? 0 : 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        return formatter.string(from: NSNumber(value: doubleValue)) ?? "$\(doubleValue)"
    }
}
